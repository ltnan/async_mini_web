import json
from mini_web import response_config


class Response:

    def return_response(self, code: int, ret_type: str, data: str):
        return response_config.response_header.format(
            response_config.response_code_map[code],
            response_config.response_content_type.get(ret_type, "text/html")) + data


class JsonResponse(Response):

    def __init__(self, data: dict):
        super().__init__()
        self.type = "application/json"
        self.data = data
        self.response_data = json.dumps(data)

    def response(self, code=200):
        return self.return_response(code, "json", json.dumps(self.data))


class HtmlResponse(Response):
    def __init__(self, data: str):
        super().__init__()
        self.type = "text/plan"
        self.data = data
        self.response_data = data

    def response(self, code=200):
        return self.return_response(code, "html", self.data)

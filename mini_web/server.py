import asyncio

from mini_web import protocol


async def server(mini_web, host="127.0.0.1", port=5695):
    loop = asyncio.get_event_loop()

    # 创建异步io服务器
    s = await loop.create_server(lambda: protocol.HttpProtocol(loop, mini_web),
                                 host=host, port=port)
    async with s:
        # 开启服务器事件循环
        await s.serve_forever()

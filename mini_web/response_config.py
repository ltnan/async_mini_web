response_header = "HTTP/1.1 {}\n" + \
                  "Server: mini_web\n" + \
                  "Transfer-Encoding: chunked  charset=UTF-8 \n" + \
                  "Content-Type: {}\n" + \
                  "Access-Control-Allow-Origin: *\n" + \
                  "\r\n"
response_content_type = {"json": "application/json", "html": "text/html"}

response_code_map = {
    101: "101 Methods Error",
    200: "200 OK",
    301: "301 Permanently Moved",
    302: "302 Temporarily Moved",
    404: "404 Not Found",
    502: "502 SERVER ERROR"
}
response_msg_map = {
    101: "<h1>101</h1><p>The Methods error or this not http request </p>",
    404: "<h1>404 not found</h1><p>The requested URL  was not found on this server.</p>",
    502:  "<h1>502</h1><p>The server error </p>"
}

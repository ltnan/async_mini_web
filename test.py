from mini_web import mini_web, response

app = mini_web.MiniWeb()


@app.route("/")
async def index(request):
    return response.JsonResponse({"code": 0, "msg": "这是首页"})


@app.route("/index", methods=["POST", "GET"])
async def hot(request):
    return response.HtmlResponse("这是热数据")

if __name__ == '__main__':
    app.run()
